import multer from 'multer';
import path from'path';
import bodyParser from 'body-parser';
import {fileURLToPath} from 'url'
import fs from 'fs';
const CURRENT_DIR = path.dirname(fileURLToPath(import.meta.url))
import  {toDASH} from './convertToDASH.mjs'



const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const {chapterNumber, pageNumber } = req.body;



    const chapterPath = `${CURRENT_DIR}/uploads/cap_${chapterNumber}`;
    const pagePath = `${CURRENT_DIR}/uploads/cap_${chapterNumber}/pag_${pageNumber}`;
    const outputPath = `${CURRENT_DIR}/processed/cap_${chapterNumber}/pag_${pageNumber}`;

  if (!fs.existsSync(chapterPath)) {
    fs.mkdirSync(chapterPath);
}

if (!fs.existsSync(pagePath)) {
    fs.mkdirSync(pagePath);
}

fs.readdir(pagePath, (err,files)=>{
  if (err) {
    console.error(`Error al leer el directorio: ${err}`);
    return;
  }



files.forEach(file => {
  const fullPath = path.join(pagePath, file);
// Comprueba si el archivo es un directorio
    fs.stat(fullPath, (err, stats) => {
      if (err) {
        console.error(`Error al obtener las estadísticas del archivo: ${err}`);
        return;
      }

      if (stats.isDirectory()) {
        console.log(`El archivo ${fullPath} es un directorio`);
      } else {
        // Aplicar la función a cada archivo
        const nameWithoutExtension = file.replace(/[.].*$/, '');

         toDASH(fullPath,outputPath,nameWithoutExtension)
      }
    });
  });
})

    cb(null, `${pagePath}`,'/ uploads/');

   

  },
  filename: (req, file, cb) => {

    cb(null, Date.now() + '-' + file.originalname);
  }
});

// Create the multer instance
const upload = multer({ storage: storage }).single('file');

export {upload} ;


