const express = require('express');
const app = express();
const port = process.env.PORT || 3001;
const bodyParser = require('body-parser');


const startServer = async () => {


const uploadModule =  await import('./upload.mjs');
const  {upload}  =  uploadModule
const expressApp = require('./express-custom');

// Set view engine to EJS
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));


// Middleware for parsing JSON request bodies
app.use(express.json());

// Middleware for serving static files
app.use(express.static('public'));

// Route for the main page
app.get('/', (req, res) => {
  res.render('index');
});

// app.use('/upload', expressApp)
app.post('/upload', upload, (req, res) => {
  const {chapterNumber, pageNumber } = req.body;



//   if (!fs.existsSync(chapterPath)) {
//     fs.mkdirSync(chapterPath);
// }

// if (!fs.existsSync(pagePath)) {
//     fs.mkdirSync(pagePath);
// }


  res.json({ message: 'File uploaded successfully!' });
});
 
// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});}
startServer();
