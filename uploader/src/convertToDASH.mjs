import ffmpeg from 'fluent-ffmpeg';
import path from 'path';
import fs from'fs';
import { exec } from 'child_process';


export function toDASH (inputDir,outputDir,name){
const ffmpegCommand = `ffmpeg -i ${inputDir} -c:v libx264 -movflags frag_keyframe+empty_moov -f dash ${outputDir}/${name}.mpd`;

    exec(ffmpegCommand, (error, stdout, stderr) => {
        if (error) {
            console.error('Error al convertir el video:', error);
        } else {
            console.log('Video convertido exitosamente. Archivos DASH generados en:', outputDir);
        }
    });
}

