import ffmpeg from 'fluent-ffmpeg';
import path from 'path';
import fs from'fs';
import { exec } from 'child_process';


export function toHLS (inputDir,outputDir,name){
  const command = ffmpeg(inputDir);

  // Configurar opciones de salida para HLS
  command
      .output(outputDir + `/${name}playlist.m3u8`)
      .addOption('-hls_time', '10') // Duración de cada segmento en segundos
      .addOption('-hls_list_size', '0') // Tamaño de la lista de reproducción (0 para infinito)
      .addOption('-hls_segment_filename', outputDirectory + `/${name}%03d.ts`) // Nombre de los segmentos

  // Ejecutar el comando
  command.run();

  console.log('Creando lista de reproducción HLS...');
}

