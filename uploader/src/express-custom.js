const express = require('express');
const expressApp = express();


expressApp.post('/', (req, res) => {
    // Content-Type: multipart/form-data;boundary=------------------------d74496d66958873e
    console.log(req.headers['content-type']);

    const boundary = req.headers['content-type'].split('boundary=')[1];

    let body = '';
    req.on('data', (chunk) => (body += chunk));

    req.on('end', () => {
        body.split(boundary).map((data, index) => console.log(index, data));

        res.sendStatus(200);
    });
});
module.exports = expressApp;

