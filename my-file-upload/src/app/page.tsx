import Image from "next/image";
import FileUpload from "./components/FileUpload";

export default function Home() {
  return (
    <main className=" bg-sky-800     w-screen h-screen grid grid-cols-3 gap-4 content-center items-center">
      <FileUpload />

    </main>
  );
}
