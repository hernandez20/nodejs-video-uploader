"use client";
import React, { useState } from 'react';
const FileUpload: React.FC = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [chapterNumber, setChapterNumber] = useState<string>('');
  const [pageNumber, setPageNumber] = useState<string>('');

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    setSelectedFile(file || null);
  };
  const handleUpload = async () => {
    if (selectedFile) {
      try {
        const formData = new FormData();
        formData.append('chapterNumber', chapterNumber);
        formData.append('pageNumber', pageNumber);
        formData.append('file', selectedFile);
   


        // Realiza la petición a la API en localhost:3000/upload
        const response = await fetch('http://localhost:3001/upload', {
          // headers:{
          //   "Content-Type":'multipart/form-data;boundary=----WebKitFormBoundaryabc123'
          // },
          method: 'POST',
          body: formData,
        });

        if (response.ok) {
          console.log('Archivo subido exitosamente');
          // Puedes manejar la respuesta de la API aquí
        } else {
          console.error('Error al subir el archivo');
          // Puedes manejar el error aquí
        }
      } catch (error) {
        console.error('Error en la petición:', error);
      }
    }
  }


  return (
    <>
      <div></div>
      <div className='flex flex-wrap -mx-3 mb-6'>

      <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">

      <input
          type="text"
          placeholder="Número de capítulo"
          value={chapterNumber}
          name='chapterNumber'
          onChange={(e) => setChapterNumber(e.target.value)}
          className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" 
        />
      </div>


      <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
 
        <input
          type="text"
          placeholder="Número de página"
          value={pageNumber}
          name='pageNumber'
          onChange={(e) => setPageNumber(e.target.value)}
          className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
        />

</div>
        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white" htmlFor="file_input">Upload file</label>

        <input type="file" onChange={handleFileChange} required className='block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400' name='file_input' />


        <button className=' mt-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded' onClick={handleUpload}>Upload</button>
      </div>
      <div></div>
    </>
  );
};

export default FileUpload;
